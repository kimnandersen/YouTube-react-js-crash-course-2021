import { useState, useEffect } from 'react'
import Header from './components/Header'
import NewTaskButton from './components/NewTaskButton'
import Tasks from './components/Tasks'
import CurrentTaskTimer from './components/CurrentTaskTimer'

function App() {
  const [tasks, setTasks] = useState([
    {
      id: 1,
      dateCreated: '07/06/2021 09:41:00',
      dateDue: '07/10/2021 10:00:00',
      title: 'Doctors Appointment',
      content: 'Doctors Appointment',
      active: false,
      finished: false,
      timeUsed: 0,
    },
    {
      id: 2,
      dateCreated: '07/01/2021 09:49:00',
      dateDue: '07/05/2021 12:00:00',
      title: 'Do Eli\'s work.',
      content: 'Be done with the flag.',
      active: false,
      finished: false,
      timeUsed: 0,
    },
    {
      id: 3,
      dateCreated: '07/02/2021 10:36:00',
      dateDue: '07/10/2021 12:00:00',
      title: 'Do Eli\'s work (Website)',
      content: 'Be done with the website.',
      active: false,
      finished: false,
      timeUsed: 0,
    }
  ])
  const [task, setCurrentTask] = useState({})

  useEffect(() => {
    const task = tasks.filter((task) => task.active === true ? task.id : '')
    if (task[0] && task[0].id) {
      const id = task[0].id
      setCurrentTask(tasks.filter((task) => task.id === id && task.active === true ? task : ''))
    } else {
      setCurrentTask({});
    }
  }, [tasks])

  function handleOnTaskToggle(id) {
     setTasks(tasks.map((task) => task.id === id ? {...task, active: !task.active } : {...task, active: false }))
  }

  function handleOnUpdateTaskTimer(id, timeToAdd) {
    console.log(`Adding ${timeToAdd} seconds to task id ${id}`)
    setTasks(tasks.map((task) => task.id === id ? {...task, timeUsed: task.timeUsed + timeToAdd} : task))
  }

  return (
    <div className="container">
     <Header />
     <NewTaskButton />

     <div className="h-1 bg-gray-300 w-screen min-w-full"></div>
     <CurrentTaskTimer onUpdate={handleOnUpdateTaskTimer} task={task} />
     <div className="h-1 bg-gray-300 w-screen min-w-full"></div>
     <Tasks tasks={tasks} onToggle={handleOnTaskToggle} />
    </div>
  );
}

export default App;
