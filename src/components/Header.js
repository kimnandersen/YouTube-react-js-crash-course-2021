import PropTypes from 'prop-types'

const Header = ({title}) => {
    return (
        <header className="w-screen min-w-max bg-gray-600 text-gray-200 px-2 pt-2 pb-2 font-semibold">
            <nav className="flex">
                <h1>{title}</h1>
            </nav>
        </header>
    )
}


Header.defaultProps = {
    title: 'Task Tracker',
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
}

export default Header
