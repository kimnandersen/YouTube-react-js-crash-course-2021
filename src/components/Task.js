const Task = ({task, onToggle}) => {
    let statusClasses = 'float-left min-w-min w-8 md:h-full h-16 min-h-full inline text-center pt-5 text-gray-100'
    let dateDue = new Date(task.dateDue)

    if (task.active && !task.finished && Date.now() < dateDue) {
        statusClasses += ' bg-green-300'
    } else if (!task.active && !task.finished && Date.now() < dateDue) {
        statusClasses += ' bg-yellow-500'
    } else if (task.finished) {
        statusClasses += ' bg-green-600'
    } else if (task.active && Date.now() > dateDue) {
        statusClasses += ' bg-red-300'
    } else if (Date.now() > dateDue) {
        statusClasses += ' bg-red-500'
    } else {
        statusClasses += ' bg-gray-400'
    }

    return (
        <>
            <article className="w-64 min-w-min border-2 bg-gray-100 dark:bg-gray-800 dark:text-white rounded p-2 m-2 flex-shrink-0 flex-grow">
                <span className={statusClasses} onDoubleClick={() => onToggle(task.id)}>{Math.ceil(task.timeUsed / 60 / 60)}</span>
                <p className="font-serif text-xs float-right">
                    Due by {new Date(task.dateDue).toLocaleString()}
                </p>                
                <br />
                <h3 key={task.id } className="text-center font-semibold">{task.title}</h3>
                <p className="font-serif text-sm text-center">
                    {task.content}
                </p>

            </article>
        </>
    )
}

export default Task