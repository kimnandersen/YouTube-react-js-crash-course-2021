import { Component } from 'react'

export default class Timer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            currentTime: 0,
            timerID: 0,
        }
    }

    componentWillUnmount() {
        clearInterval(this.state.timerID)
        if (this.state.currentTime > 0) {
            this.props.onUpdate(this.props.task.id, this.state.currentTime)
        }
    }

    componentDidUpdate(prevProps) {
        console.log(this.props);
        console.log(prevProps);
        if (this.props.task.length > 0 && this.state.timerID === 0) {
            if (prevProps.task.length > 0 && this.props.task[0].id !== prevProps.task[0].id && this.state.currentTime > 0) {
                this.props.onUpdate(prevProps.task[0].id, this.state.currentTime);
                this.setState(() => {
                    return {currentTime: 0}
                });
            }
            clearInterval(this.state.timerID)
            this.setState(() => {
                return {currentTime: 0, timerID: 0}
            });
        }
        if (this.props.task.length >= 1 && this.state.timerID === 0) {
            this.setState((state) => {
                if (state.timerID !== 0) {
                    return;
                } else {
                    return {timerID: setInterval(() => {this.setState((state) => {return {currentTime: state.currentTime + 1}})}, 1000)}
                }
            });
        } else if (this.props.task.length < 1 && this.state.timerID !== 0) {
            clearInterval(this.state.timerID);
            this.setState(() => {
                return {timerID: 0, currentTime: 0};
            });
        }
    }



    render() {
        return (
            <>
                <h3>Timer: {this.state.currentTime}</h3>
            </>
        )
    }
}
