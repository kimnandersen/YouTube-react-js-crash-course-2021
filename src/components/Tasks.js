import Task from './Task'

const Tasks = ({tasks, onToggle}) => {
    return (
        <>
            <div className="min-w-full w-screen pt-4 px-4 flex">
                <div className="md:flex justify-around min-w-full w-6/12 p-2 mx-auto">
                    {tasks.map((task) => {
                        return (<Task key={task.id} task={task} onToggle={onToggle} />)
                    })}
                </div>
            </div>
        </>
    );
}

export default Tasks