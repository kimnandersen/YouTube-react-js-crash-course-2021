import { useState } from 'react'
import { Dialog } from '@headlessui/react'

const NewTaskButton = () => {
    let [isOpen, setIsOpen] = useState(false);

    function handleAddNewTask() {
        setIsOpen(false);
    }

    function clickAddNewTask() {
        setIsOpen(true);
    }

    return (
        <>
            <div className="m-4 w-screen min-w-full flex justify-center">
                <button onClick={clickAddNewTask} className="w-6/12 bg-green-400 rounded shadow-sm p-2 hover:bg-green-300 transform active:scale-110 active:bg-green-700"><span>Add New Task!</span></button>
            </div>

            <Dialog open={isOpen} onClose={() => setIsOpen(false)} className='fixed inset-0 z-10 overflow-y-auto'>
                <div className="flex items-center justify-center min-h-screen">
                    <Dialog.Overlay className='fixed inset-0 bg-black opacity-30' />
                    <span
                        className="inline-block h-screen align-middle"
                        aria-hidden="true"
                    >
                        &#8203;
                    </span>
                    <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
                        <Dialog.Title className="text-black font-semibold rounded text-center">Add New Task!</Dialog.Title>
                        <div className="bg-gray-300 max-w-sm min-w-max"></div>
                        <Dialog.Description className="bg-gray-300 text-xs rounded text-center" as="div">
                            This will allow you to add a new task.
                        </Dialog.Description>

                        <p className="p-2 min-w-full w-full">
                            <span className="text-sm text-center w-full min-w-full inline-block">Title</span>
                            <br />
                            <input type="text" id="title" className="rounded border-2 border-yellow-700 min-w-full w-full" />
                            <br />
                            <span className="text-sm text-center w-full min-w-full inline-block">Date Due</span>
                            <br />
                            <div className="rounded border-2 border-yellow-700 min-w-full w-full flex justify-between">
                                <input type="date" id="date" />
                                <input type="time" id="time" />
                            </div>
                            <br /><br />
                            <span className="text-sm text-center inline-block min-w-full w-full">Content</span>
                            <br />
                            <textarea rows="5" id="body" className="rounded border-2 border-yellow-700 min-w-full w-full flex-none" />
                        </p>

                        <div className="p-2">
                            <button onClick={handleAddNewTask} className="rounded bg-green-300 p-2 w-24 hover:bg-green-700 hover:text-white transform motion-safe:active:scale-110 active:bg-green-900">Add</button>
                            <button onClick={() => setIsOpen(false)} className="float-right rounded bg-red-300 p-2 w-24 hover:bg-red-700 hover:text-white transform motion-safe:active:scale-110 active:bg-red-900">Cancel</button>
                        </div>
                    </div>
                </div>
            </Dialog>
        </>
    );
}

export default NewTaskButton;
