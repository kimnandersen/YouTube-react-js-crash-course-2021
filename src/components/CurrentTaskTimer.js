import Timer from './Timer'

const CurrentTaskTimer = ({task, onUpdate}) => {
    return (
        <>
            <div className="w-screen min-w-full flex justify-center">
                <div className="w-10/12 min-w-max h-24 bg-gray-100 rounded text-center">
                    <h3>Current Task: <span className={`text-sm w-4/12 inline-block ${task.length > 0 ? 'bg-green-300 text-black' : 'bg-gray-500 text-white'}`}>{task.length > 0 ? task[0].title : 'None'}</span></h3>
                    <div className="w-full min-w-full h-2 bg-white"></div>
                    <Timer task={task} onUpdate={onUpdate} />
                </div>

            </div>
        </>
    )
}

export default CurrentTaskTimer
